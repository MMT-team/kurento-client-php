<?php

use \KCS\KCS;

class KCSTest extends PHPUnit_Framework_TestCase
{
	/**
	 * @var KCS
	 */
	private $kcs;

	public function setUp(){
		$url = 'http://localhost:8080';
		$apiKey = '1';
		$apiSecret = 'testPrivateKey';

		$this->kcs = new KCS($url, $apiKey, $apiSecret);
	}

	public function testCreateSessionAndToken(){
		$dataSession = $this->kcs->createSession();
		var_dump($dataSession);

		$this->assertArrayHasKey('session_id', $dataSession);
		$session_id = $dataSession['session_id'];

		$dataToken = $this->kcs->createToken($session_id);
		var_dump($dataToken);
		$this->assertArrayHasKey('token_id', $dataToken);

		$dataMedia = $this->kcs->getMedia($session_id);
		var_dump($dataMedia);

		$data = $this->kcs->closeSession($session_id);
		var_dump($data);

		$data = $this->kcs->getSession($session_id);
		$this->assertArrayHasKey('session_id', $data);
		var_dump($data);
	}
}
