<?php

namespace KCS\Util;

use \GuzzleHttp\Exception\ClientException;
use \GuzzleHttp\Exception\ServerException;
use KCS\KcsException;

class Client extends \GuzzleHttp\Client
{

	/** Создаем сессию
	 * @param $options
	 * @return bool
	 */
	public function createSession($options){
		try {
			$data = $this->post('/session', ['json' => $options])->json();
		} catch(\Exception $e){
			$this->handleException($e);
			return false;
		}
		return $data;
	}

	/** Закрываем сессию
	 * @param $options
	 * @return mixed|void
	 */
	public function closeSession($options){
		try {
			$data = $this->put('/session/close', ['json' => $options])->json();
		} catch (\Exception $e){
			$this->handleException($e);
			return false;
		}
		return $data;
	}

	/** Создаем токен
	 * @param $options
	 * @return bool
	 */
	public function createToken($options){
		try {
			$data = $this->post('/token', ['json' => $options])->json();
		} catch(\Exception $e){
			$this->handleException($e);
			return false;
		}
		return $data;
	}

	/** Получаем информацию о сессии
	 * @param $options
	 * @return bool
	 */
	public function getSession($options){
		try {
			$data = $this->get('/session', ['query' => $options])->json();
		} catch (\Exception $e){
			$this->handleException($e);
			return false;
		}
		return $data;
	}

	/**
	 * Получаем медиа
	 * @param $options
	 * @return bool|mixed
	 * @throws KcsException
	 */
	public function getMedia($options){
		try {
			$data = $this->get('/session/media', ['query' => $options])->json();
		} catch (\Exception $e){
			$this->handleException($e);
			return false;
		}
		return $data;
	}

	/** Обработчик ошибок
	 * @param $e
	 * @throws KcsException
	 */
	private function handleException($e){
		if ($e instanceof ClientException){
			if ($e->getResponse()->getStatusCode() == 403){
				throw new KcsException('auth error', KcsException::AUTH_FAILED, $e);
			} else {
				throw new KcsException('request error', KcsException::REQUEST_FAILED, $e);
			}
		} else if ($e instanceof ServerException){
			throw new KcsException('server error', KcsException::SERVER_ERROR, $e);
		} else {
			throw new KcsException('unknown error', KcsException::UNKNOWN_ERROR, $e);
		}
	}
}
