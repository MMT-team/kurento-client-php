<?php

namespace KCS;

class KcsException extends \Exception
{
    const AUTH_FAILED = 1;
    const REQUEST_FAILED = 2;
    const SERVER_ERROR = 3;
    const UNKNOWN_ERROR = 4;

}
