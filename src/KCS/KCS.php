<?php

namespace KCS;

use KCS\Util\Client;

class KCS
{
	/**
	 * @var Client
	 */
	private $client;

	const ROLE_PUBLISHER = 'publisher';
	const ROLE_SUBSCRIBER = 'subscriber';
	const ROLE_MODERATOR = 'moderator';

	const TRANSPORT_WEBRTC = 'webrtc';
	const TRANSPORT_SIP = 'sip';

	public function __construct($url, $apiKey, $apiSecret, $options = []) {

		$this->url = $url;
		$this->apiKey = $apiKey;
		$this->apiSecret = $apiSecret;

		$this->client = new Client([
			'base_url' => $url,
		    'defaults' => [
			    'auth' => [$apiKey, $apiSecret]
		    ]
		]);
	}

	/**
	 * @param array $options
	 * @return mixed|void
	 * @throws \Exception
	 */
	public function createSession($options = []){

		$defaults = [
			'transport' => self::TRANSPORT_WEBRTC,
			'transport_options' => new \stdClass()
		];
		$options = array_merge($defaults, array_intersect_key($options, $defaults));
		return $this->client->createSession($options);
	}

	public function closeSession($sessionId, $options = []){
		$defaults = [
			'message' => 'default message'
		];
		$options = array_merge($defaults, array_intersect_key($options, $defaults));
		$options['session_id'] = $sessionId;

		return $this->client->closeSession($options);
	}

	/**
	 * @param $sessionId
	 * @param array $options
	 * @return mixed|void
	 * @throws \Exception
	 */
	public function createToken($sessionId, $options = []){

		$defaults = [
			'role' => self::ROLE_PUBLISHER,
		    'expire_time' => null,
		    'metadata' => null
		];

		$options = array_merge($defaults, array_intersect_key($options, $defaults));
		$options['session_id'] = $sessionId;

		return $this->client->createToken($options);
	}

	/**
	 * @param $sessionId
	 * @param array $options
	 * @return bool
	 */
	public function getSession($sessionId, $options = []){
		$options['session_id'] = $sessionId;
		return $this->client->getSession($options);
	}

	/**
	 * @param $sessionId
	 * @param array $options
	 * @return bool|mixed
	 */
	public function getMedia($sessionId, $options = []){
		$options['session_id'] = $sessionId;
		$options['direct'] = 0;
		return $this->client->getMedia($options);
	}
}
